package com.formation.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Table(name="score")
@Component
@Scope("prototype")
public class Score {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int year;
	private int sport_score;
	private int social_score;
	private int performance_score;
	private int attendance_score;
	
	@ManyToOne
	@JoinColumn(name="employee_id")
	private Employee employee;
	
	public Score() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getSport_score() {
		return sport_score;
	}

	public void setSport_score(int sport_score) {
		this.sport_score = sport_score;
	}

	public int getSocial_score() {
		return social_score;
	}

	public void setSocial_score(int social_score) {
		this.social_score = social_score;
	}

	public int getPerformance_score() {
		return performance_score;
	}

	public void setPerformance_score(int performance_score) {
		this.performance_score = performance_score;
	}

	public int getAttendance_score() {
		return attendance_score;
	}

	public void setAttendance_score(int attendance_score) {
		this.attendance_score = attendance_score;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
}
