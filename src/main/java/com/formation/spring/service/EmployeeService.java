package com.formation.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.stereotype.Service;

import com.formation.spring.model.Employee;
import com.formation.spring.repo.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	public void createOrUpdateEmployee(Employee emp) {
		this.employeeRepository.save(emp);
	}
	
	public void deleteUser(Employee emp) {
		this.employeeRepository.delete(emp);
	}
	
	public List<Employee> findAll() {
		return this.employeeRepository.findAll();
	}

	public Page<Employee> findAll(Pageable pageable) {
		return this.employeeRepository.findAll(pageable);
	}
	
	public List<Employee> findByNom(String nom) {
		return this.employeeRepository.findByNom(nom);
	}
	public Page<Employee> findByNom(String nom, Pageable pageable) {
		return this.employeeRepository.findByNom(nom, pageable);
	}
	
	public float getAverageScores(int id, int year) {
		return this.employeeRepository.getAverageScores(id, year);
	}
}
