package com.formation.spring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.formation.spring.config.BeanConfig;
import com.formation.spring.config.JPAConfig;
import com.formation.spring.model.Address;
import com.formation.spring.model.Employee;
import com.formation.spring.model.Score;
import com.formation.spring.service.EmployeeService;
import com.sun.xml.bind.CycleRecoverable.Context;




public class App 
{

	
    public static void main( String[] args )
    {
    	
    	System.out.println("==== POPULATE DATABASE WITH 40 ENTRIES ====");
    	AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    	context.register(BeanConfig.class, JPAConfig.class);
    	context.refresh();
    	
    	for (int i = 0; i < 40; i++) {
    		Employee emp1 = context.getBean(Employee.class);
        	emp1.setLogin("mylogin" + i);
        	emp1.setPrenom("Willy" + i);
        	emp1.setNom("Wonka" + i);
        	emp1.setEmail("willy.wonka" + i + "@gmail.com");
        	emp1.setPassword("1234" + i);
        	emp1.setRole("Sales Director");

        	Address addr1 = context.getBean(Address.class);
            addr1.setNumber(18+i);
            addr1.setStreet("rue du hiboux" + i);
            addr1.setPostalCode("9320" + i);
            addr1.setCity("Saint Denis");
            addr1.setCountry("France");
            addr1.setEmployee(emp1);
            Address addr2 = context.getBean(Address.class);
            addr2.setNumber(8);
            addr2.setStreet("rue du hiboux");
            addr2.setPostalCode("33200");
            addr2.setCity("Sat Denis");
            addr2.setCountry("Frace");
            addr2.setEmployee(emp1);
            
            
            List<Address> addresses = new ArrayList<Address>();
            addresses.add(addr1);
            addresses.add(addr2);
            emp1.setAddresses(addresses);
            
            Score score1 = context.getBean(Score.class);
            score1.setYear(2019);
            score1.setSport_score(8);
            score1.setSocial_score(6);
            score1.setPerformance_score(8);
            score1.setAttendance_score(4);

            score1.setEmployee(emp1);
            emp1.setScores(Arrays.asList(score1));
            
            EmployeeService empService = (EmployeeService)context.getBean(EmployeeService.class);
            empService.createOrUpdateEmployee(emp1);
            System.out.println(emp1);
    	}    
    	
    	EmployeeService empService = (EmployeeService)context.getBean(EmployeeService.class);
    	
    	// Get paginate of 20 employes
    	Pageable firstPageWithTwentyElements = PageRequest.of(0, 20);
    	Page<Employee> employees = empService.findAll(firstPageWithTwentyElements);
    	System.out.println("=== Page of 20 employes ===");
    	for (Employee employee : employees) {
			System.out.println(employee.getNom());
		}
//           
    		
            System.out.println("La moyenne du premier employe est " +  empService.getAverageScores(1, 2019));  
		
 
    }
 
    
    
}
