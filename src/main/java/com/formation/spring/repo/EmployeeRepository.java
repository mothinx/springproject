package com.formation.spring.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.formation.spring.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
		public List<Employee> findByNom(String n);
		public Page<Employee> findByNom(String n, Pageable pageable);
		public Page<Employee> findAll(Pageable pageable);
		@Query(value = "SELECT AVG((attendance_score + performance_score + social_score + sport_score) / 4) as moyenne " + 
				"FROM score " + 
				"WHERE employee_id = ?1 AND year = ?2", nativeQuery = true)
		public float getAverageScores(int id, int year);		
}
